#!/bin/sh
id="$(docker ps | grep 'recruiterbox-assignment_server' | awk '{ print $1 }')"
docker exec -t -i "${id}" pip install psycopg2
docker exec -t -i "${id}" python manage.py makemigrations
docker exec -t -i "${id}" python manage.py migrate

id="$(docker ps | grep 'recruiterbox-assignment_server' | awk '{ print $1 }')"
docker exec -t -i "${id}" python manage.py createsuperuser
