import jwt
from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.models import User
from customauth.models import Employee, Company
from utils.decorators import get_token_data

def index(request):
    return JsonResponse({})

@get_token_data
def employees(request, token_data):
	if request.method == 'GET':
		employees = Employee.objects.filter(company__id = token_data['company_id'])
		employees_list = []
		for employee in employees:
			employees_list.append({
				'name': employee.user.first_name,
				'id': employee.id,
				'thumbnail': employee.thumbnail
				})
		return JsonResponse({'valid':False, 'data':{'list': employees_list}})

def companies(request):
	if request.method == 'GET':
		companies = Company.objects.all()
		companies_list = []
		for company in companies:
			companies_list.append({
				'name': company.name,
				'id': company.id,
				})
		return JsonResponse({'valid':False, 'list': companies_list})