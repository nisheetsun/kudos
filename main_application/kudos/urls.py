from django.urls import path

from . import views

urlpatterns = [
    path('', views.kudos, name='kudos'),
]