from django.http import HttpResponse, JsonResponse
from .models import Kudos
from django import forms, db
from customauth.models import Employee
from utils.cache import get, set_cache
from utils.decorators import get_token_data, get_body
from utils.validators import KudosPostForm
from main_application.settings import USER_KUDOS_KEY

# Pf7LiWH!4H83sC

def index(request):
    return HttpResponse("Hello, world. You're at the kudos index.")

@get_token_data
@get_body
def kudos(request, token_data, body={}):
	
	if request.method == 'GET':
		kudos = Kudos.objects.filter(to_employee=token_data['id'])
		kudos_list = []
		for kudo in kudos:
			kudos_list.append({'from': kudo.from_employee.user.username, 'message': kudo.message})
		return JsonResponse({'valid':True, 'data': {'list':kudos_list}})
	
	elif request.method == 'POST':
		form = KudosPostForm(body)
		if form.is_valid() is False:
			return JsonResponse({'valid':False, 'message':'invalid body'})
		key = USER_KUDOS_KEY+ str(token_data['id'])
		available = get(key)
		if available:
			set_cache(key, available-1)
			from_employee = Employee.objects.filter(id=token_data['id'])[0]
			message = body.get("message", '')
			to_employee = body.get("to_employee")
			to_employee = Employee.objects.filter(id=to_employee)[0]
			if from_employee.id != to_employee.id:
				kudos = Kudos(message=message, from_employee=from_employee, to_employee=to_employee)
				kudos.save()
				return JsonResponse({'valid':True, 'data': {'body':body, 'available_kudos': available-1}})
			else:
				return JsonResponse({'valid':False, 'message': 'cannot kudos self'})
		else:
			return JsonResponse({'valid':False, 'message': 'kudos limit exceeded'})
	return JsonResponse({'valid':False, 'message': 'something went wrong'})

