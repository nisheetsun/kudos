from django.db import models
from django.contrib.auth.models import User
from customauth.models import Employee

class Kudos(models.Model):
	id = models.AutoField(primary_key=True)
	message = models.CharField(max_length=100)
	from_employee = models.ForeignKey(Employee, on_delete=models.CASCADE, null=False, related_name='%(class)s_from_employee')
	to_employee = models.ForeignKey(Employee, on_delete=models.CASCADE, null=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		string = str(self.from_employee)+" TO "+str(self.to_employee)+" WITH MESSAGE "+str(self.message)
		return string
