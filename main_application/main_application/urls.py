from django.contrib import admin
from django.urls import path, include

urlpatterns = [
	path('kudos/', include('kudos.urls')),
    path('auth/', include('customauth.urls')),
    path('employee/', include('employee.urls')),
    path('admin/', admin.site.urls),

]
