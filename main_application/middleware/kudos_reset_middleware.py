import time
from utils.cache import get, reset_kudos, set_cache
from main_application.settings import KUDOS_RESET_KEY, KUDOS_RESET_DURATION


def kudos_middleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        last_updated = get(KUDOS_RESET_KEY)
        current_timestamp = int(round(time.time()))
        if last_updated:
        	if current_timestamp-last_updated >= KUDOS_RESET_DURATION:
        		set_cache(KUDOS_RESET_KEY, current_timestamp)
        		reset_kudos()
        else:
        	set_cache(KUDOS_RESET_KEY, current_timestamp)
        	reset_kudos()
        response = get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    return middleware