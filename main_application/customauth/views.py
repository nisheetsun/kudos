import json
import jwt
import time
from django.http import JsonResponse
from django.contrib.auth.models import User
from django import forms, db
from django.contrib.auth import authenticate
from .models import Employee, Company
from django.http import JsonResponse
from django.core.cache import cache
from utils.cache import get, set_cache, reset_kudos
from main_application.settings import SECRET_KEY, KUDOS_LIMIT, USER_KUDOS_KEY
from utils.validators import SignupForm, LoginForm
from utils.decorators import get_body


def index(request):
    return JsonResponse({})

@get_body
def login(request, body={}):
	if request.method == 'POST':
		form = LoginForm(body)
		if form.is_valid():
			user = authenticate(username=body['email'], password=body['password'])
			if user:
				employee = Employee.objects.filter(user__username = body['email'])
				encoded_jwt = jwt.encode({'id': employee[0].id, 'company_id':employee[0].company_id, 'timestamp': time.time()}, SECRET_KEY, algorithm='HS256')
				return JsonResponse({'valid':True, 'message': 'success', 'data':{'token': encoded_jwt.decode("utf-8"), 'email': employee[0].user.username}})
			else:
				return JsonResponse({'valid':False, 'message':'user not present'})
		else:
			return JsonResponse({'valid':False, 'message':'all keys not available'})
		return JsonResponse({'valid':False})


def post_employee(body):
	user = User.objects.create_user(body['email'])
	user.first_name = body['name']
	user.set_password(body['password'])
	user.save()
	employee = Employee(user=user, company_id=body['company_id'])
	employee.save()
	return str(employee.id)

@get_body
def signup(request, body={}):
	if request.method == 'POST':
		form = SignupForm(body)
		company = Company.objects.filter(id = body['company_id'])
		if not company:
			return JsonResponse({'valid':False, 'message':'company not present'})
		if form.is_valid():
			try:
				key = post_employee(body)
				set_cache(USER_KUDOS_KEY + key, KUDOS_LIMIT)
				return JsonResponse({'message': 'success'})
			except db.utils.IntegrityError as e:
				return JsonResponse({'valid':False, 'message': 'user already present'})

		else:
			return JsonResponse({'valid':False, 'message':'body is missing or invalid'})
		return JsonResponse({'valid':False, 'message':'something went wrong'})
