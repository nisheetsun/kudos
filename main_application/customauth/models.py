from django.db import models

# Create your models here.

from django.contrib.auth.models import User

class Company(models.Model):
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=100, null=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Employee(models.Model):
	id = models.AutoField(primary_key=True)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	thumbnail = models.CharField(max_length=100, null=False)
	company = models.ForeignKey(Company, on_delete=models.CASCADE, null=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.user.username