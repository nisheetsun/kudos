from django.core.cache import cache
from main_application.settings import KUDOS_LIMIT, USER_KUDOS_KEY

def set_cache(key, value, timeout=None):
	if timeout:
		return cache.set(key, value)
	else:
		return cache.set(key, value, timeout)

def get_all():
	return cache.keys('*')

def get(key):
	return cache.get(key)

def reset_kudos():
	all = get_all()
	for key in all:
		if USER_KUDOS_KEY in key:
			set_cache(key, KUDOS_LIMIT)