import json
import jwt
from main_application.settings import SECRET_KEY
from django.http import HttpResponse, JsonResponse


def get_body(function):
	def wrapper(*args, **kwargs):
		try:
			request = args[0]
			body_unicode = request.body.decode('utf-8')
			body = json.loads(body_unicode)
		except Exception as e:
			body = {}
		return function(*args, **kwargs, body = body)
	return wrapper

def get_token_data(function):
	def wrapper(*args, **kwargs):
		try:
			request = args[0]
			headers = request.headers
			Authorization = headers['Authorization']
			token_data = jwt.decode(Authorization, SECRET_KEY, algorithms=['HS256'])
			return function(*args, **kwargs, token_data = token_data)
		except KeyError:
			return JsonResponse({'message':'no token'})
		except jwt.exceptions.DecodeError:
			return JsonResponse({'message':'invalid token'})
	return wrapper
