from django import forms, db

class SignupForm(forms.Form):
    name = forms.CharField(required=True)
    password = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    company_id = forms.IntegerField(required=True)

class LoginForm(forms.Form):
    password = forms.CharField(required=True)
    email = forms.EmailField(required=True)

class KudosPostForm(forms.Form):
	to_employee = forms.IntegerField(required=True)
	message = forms.CharField()