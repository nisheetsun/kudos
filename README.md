## Config Variables

These are the cconfig variables defined in setting file

* KUDOS_LIMIT - Limit of kudos per time period
* USER_KUDOS_KEY - Redis key prefix to store remaining kudos of a user
* KUDOS_RESET_KEY - Redis key to store timestamp of last reset kudos operation.
* KUDOS_RESET_DURATION - Time period to reset the kudos

---